import React from 'react';
import {Container, Header, Segment} from "semantic-ui-react";

const Impressum = () => {
    return(
        <Segment vertical>
            <Container>
                <Header as='h3' style={{fontSize: '2em'}}>
                    Impressum
                </Header>
            </Container>
        </Segment>
    );
};

export default Impressum;