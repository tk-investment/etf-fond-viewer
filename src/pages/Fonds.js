import React from 'react';
import {Container, Header, Segment} from "semantic-ui-react";

const Fonds = () => {
    return(
        <Segment vertical>
            <Container>
                <Header as='h3' style={{fontSize: '2em'}}>
                    Fonds
                </Header>
            </Container>
        </Segment>
    );
};

export default Fonds;