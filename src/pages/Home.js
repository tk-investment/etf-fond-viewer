import React from 'react';
import {Container, Header, Segment} from "semantic-ui-react";

const Home = () => {
    return(
        <Segment vertical>
            <Container>
                <Header as='h3' style={{fontSize: '2em'}}>
                    Home
                </Header>
            </Container>
        </Segment>
    );
};

export default Home;