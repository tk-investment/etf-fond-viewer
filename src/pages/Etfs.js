import React from 'react';
import {Container, Header, Segment} from "semantic-ui-react";
import EtfView from "../components/etf/EtfView";

const Etfs = () => {
    return (
        <Segment vertical>
            <Container>
                <Header as='h3' style={{fontSize: '2em'}}>
                    ETFs
                </Header>
                <EtfView/>
            </Container>
        </Segment>
    );
};

export default Etfs;