import React from 'react'
import {Table} from 'semantic-ui-react'


function EtfTable(props) {
    const tableHeader = props.tableHeader;
    const etfs = props.etfs;

    const morningStarLink = (id) => {
        const link = 'https://www.morningstar.de/de/etf/snapshot/snapshot.aspx?id=';
        if (id) {
            return (<a href={link + id} rel="noopener noreferrer" target='_blank'>{id}</a>);
        }
        return <></>;
    };

    const comdirectLink = (isin) => {
        const link = 'https://www.comdirect.de/inf/etfs/';
            return (<a href={link + isin} rel="noopener noreferrer" target='_blank'>comdirect</a>);
    };

    const providerLink = (provider, isin, name) => {
        if (provider === 'Amundi') {
            const link = 'https://www.amundietf.de/privatkunden/product/view/';
            return (<a href={link + isin} rel="noopener noreferrer" target='_blank'>{name}</a>);

        } else if (provider=== 'Lyxor') {
            const link = 'https://www.lyxoretf.de/de/retail/products/equity-etf/comstage-1-dax-ucits-etf-i/de000etf9017/eur';
            return name;

        } else if (provider === 'Vanguard') {
            const link = '';
            return name;
        }
    };


    return (
        <Table celled>
            <Table.Header>
                <Table.Row>
                    {
                        tableHeader.map(item => {
                            return (
                                <Table.HeaderCell
                                    key={item}>
                                    {item}
                                </Table.HeaderCell>
                            )
                        })
                    }

                </Table.Row>
            </Table.Header>

            <Table.Body>
                {
                    etfs.map(etf => {
                        return (
                            <Table.Row key={etf.isin}>
                                <Table.Cell>{etf.provider}</Table.Cell>
                                <Table.Cell>{etf.isin}</Table.Cell>
                                <Table.Cell>{providerLink(etf.provider, etf.isin, etf.name)}</Table.Cell>
                                <Table.Cell>{morningStarLink(etf.morningStarId)}</Table.Cell>
                                <Table.Cell>{comdirectLink(etf.isin)}</Table.Cell>
                            </Table.Row>
                        );
                    })
                }

            </Table.Body>
        </Table>
    );
}

export default EtfTable;