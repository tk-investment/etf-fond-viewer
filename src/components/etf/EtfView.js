import React from 'react';
import Etfs from './etfs';
import EtfTable from "./EtfTable";
import {Input} from "semantic-ui-react";

class EtfView extends React.Component {

    etfsWithSearchTerm = Etfs.map(etf => {
        return {
            ...etf,
            searchString: JSON.stringify(etf).toLowerCase(),
        };
    });

    constructor(props) {
        super(props);
        this.state = {
            header: ['Anbieter', 'ISIN', 'Name (Anbieter Link)', 'Morning Star', 'comdirect'],
            data: this.etfsWithSearchTerm,
            searchTerm: '',
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const searchTerm = event.target.value;

        const filteredData = searchTerm === '' ? this.etfsWithSearchTerm :
            this.etfsWithSearchTerm.filter(etf => etf.searchString.includes(searchTerm.toLowerCase()));

        this.setState({
            data: filteredData,
            searchTerm: searchTerm,
        });
    }

    render() {
        return (
            <>
                <Input
                    fluid icon='search'
                    placeholder='Search ...'
                    value={this.state.searchTerm}
                    onChange={this.handleChange}
                />
                <EtfTable
                    tableHeader={this.state.header}
                    etfs={this.state.data}/>
            </>
        );
    }

}

export default EtfView;

